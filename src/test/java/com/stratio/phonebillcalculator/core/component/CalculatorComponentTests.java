package com.stratio.phonebillcalculator.core.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.stratio.phonebillcalculator.TestsHelper;
import com.stratio.phonebillcalculator.config.CalculatorConfiguration;
import com.stratio.phonebillcalculator.core.component.impl.CalculatorComponentImpl;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.core.exceptions.InvalidTimeFormat;

@RunWith(MockitoJUnitRunner.class)
class CalculatorComponentTests {

	@Mock
	public CalculatorConfiguration conf;

	@InjectMocks
	public CalculatorComponentImpl cmp;

	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void init_works() {
		assertThat(cmp).isNotNull();
	}

	@Test
	void getMinutes_returnsOneMinuteMore_minuteIsNotComplete() {
		assertThat(cmp.getCeilMinutes(61)).isEqualTo(2);
		assertThat(cmp.getCeilMinutes(1)).isEqualTo(1);
		assertThat(cmp.getCeilMinutes(119)).isEqualTo(2);
		assertThat(cmp.getCeilMinutes(0)).isZero();
	}

	@Test
	void findOutLongerCall_returnsTheLongest_multipleRecords() {
		List<PhoneBillLineCalculation> lines = new ArrayList<>();
		TestsHelper.addToList("000-000-002", 50, lines);
		TestsHelper.addToList("000-000-001", 100, lines);
		TestsHelper.addToList("000-000-001", 200, lines);
		TestsHelper.addToList("000-000-002", 50, lines);
		TestsHelper.addToList("000-000-003", 150, lines);

		assertThat(cmp.findOutLongestCall(lines).getTelephoneNumber()).isEqualTo("000-000-001");
	}

	@Test
	void findOutLongerCall_returnsTheLongest_oneTelephoneNumber() {
		List<PhoneBillLineCalculation> lines = new ArrayList<>();
		TestsHelper.addToList("000-000-001", 50, lines);
		TestsHelper.addToList("000-000-001", 100, lines);
		TestsHelper.addToList("000-000-001", 200, lines);

		assertThat(cmp.findOutLongestCall(lines).getTelephoneNumber()).isEqualTo("000-000-001");
	}

	@Test
	void findOutLongerCall_returnsTheLongestAndTheSmaller_mutipleTelephoneNumbersSameLength() {
		List<PhoneBillLineCalculation> lines = new ArrayList<>();
		TestsHelper.addToList("100-100-001", 50, lines);
		TestsHelper.addToList("100-100-001", 100, lines);
		TestsHelper.addToList("100-100-001", 200, lines);
		TestsHelper.addToList("101-000-001", 50, lines);
		TestsHelper.addToList("100-120-001", 50, lines);
		TestsHelper.addToList("110-110-001", 100, lines);
		TestsHelper.addToList("102-100-001", 200, lines);
		TestsHelper.addToList("101-000-001", 100, lines);
		TestsHelper.addToList("101-000-001", 200, lines);

		assertThat(cmp.findOutLongestCall(lines).getTelephoneNumber()).isEqualTo("100-100-001");
	}

	@Test
	void calculatePhoneBillLineCost_shouldCalculate_cost1() {
		when(conf.getLength()).thenReturn(200);
		when(conf.getCost1()).thenReturn(new BigDecimal(3));

		assertThat(cmp.calculatePhoneBillLineCost(199))
				.isEqualByComparingTo(conf.getCost1().multiply(new BigDecimal(199)));
	}

	@Test
	void calculatePhoneBillLineCost_shouldCalculate_cost2() {
		when(conf.getLength()).thenReturn(5 * 60);
		when(conf.getCost1()).thenReturn(new BigDecimal(3));
		when(conf.getCost2()).thenReturn(new BigDecimal(30));

		// 5 minutes and 1 second should be 6 minutes
		assertThat(cmp.calculatePhoneBillLineCost(301)).isEqualByComparingTo(new BigDecimal(6 * 30));
	}

	@Test
	void validateTelephoneNumber_shoudValidate_telephoneNumberPattern() {
		assertThat(cmp.validateTelephoneNumber("000-000-000")).isTrue();
	}

	@Test
	void validateTelephoneNumber_shoudValidate_telephoneLenghtLength() {
		assertThat(cmp.validateTelephoneNumber("00-000-000")).isFalse();
	}

	@Test
	void validateTelephoneNumber_shoudValidate_telephoneNumberHasOnlyNumbers() {
		assertThat(cmp.validateTelephoneNumber("A0-000-000")).isFalse();
	}

	@Test
	void validateTelephoneNumber_shoudNotValidate_noLeadingZeros() {
		assertThat(cmp.validateTelephoneNumber("10-000-000")).isFalse();
	}

	@Test
	void getSecondsFromString_shouldReturnSeconds_patternIsCorrect() throws InvalidTimeFormat {
		assertThat(cmp.getSecondsFromString("01:01:01")).isEqualTo(3661);
	}

	@Test
	void getSecondsFromString_shouldThrowsException_patternIsNotCorrect() {
		Assert.assertThrows(InvalidTimeFormat.class, () -> cmp.getSecondsFromString("-01:01:01"));
	}

	@Test
	void findOutTelephoneNumberForTheLongestCall_returnsSmall_serveralTlfs() {
		assertThat(
				cmp.findOutTelephoneNumberForTheLongestCall(Arrays.asList("201-231-022", "001-001-001", "011-001-001")))
						.isEqualTo("001-001-001");
	}
}
