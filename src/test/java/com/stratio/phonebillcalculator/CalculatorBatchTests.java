package com.stratio.phonebillcalculator;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.exceptions.CalculatorJobException;
import com.stratio.phonebillcalculator.facade.CalculatorFacade;

@SpringBootTest
class CalculatorBatchTests {

	@Autowired
	private CalculatorFacade calculatorFacade;

	@Test
	void endToEndBatch_case1() throws CalculatorJobException {
		String path = "src/test/resources/testdata1.txt";
		PhoneBillCalculation result = calculatorFacade.createJob(path);
		assertThat(result).isNotNull();
		assertThat(result.getTotalCost()).isEqualByComparingTo(new BigDecimal(900));
	}

	@Test
	void endToEndBatch_case2() throws CalculatorJobException {
		String path = "src/test/resources/testdata2.txt";
		PhoneBillCalculation result = calculatorFacade.createJob(path);
		assertThat(result).isNotNull();
		assertThat(result.getTotalCost()).isEqualByComparingTo(new BigDecimal(1170));
	}

	@Test
	void endToEndBatch_case3() throws CalculatorJobException {
		String path = "src/test/resources/testdata3.txt";
		PhoneBillCalculation result = calculatorFacade.createJob(path);
		assertThat(result).isNotNull();
		assertThat(result.getTotalCost()).isEqualByComparingTo(new BigDecimal(201 + 750 + 900));
	}
}
