package com.stratio.phonebillcalculator;

import java.util.List;

import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;

public class TestsHelper {

	private TestsHelper() {
		// No instance
	}
	
	public static void addToList(String tln, int seconds, List<PhoneBillLineCalculation> line) {
		PhoneBillLineCalculation line1 = new PhoneBillLineCalculation();
		line1.setTelephoneNumber(tln);
		line1.setLength(seconds);

		line.add(line1);
	}
}
