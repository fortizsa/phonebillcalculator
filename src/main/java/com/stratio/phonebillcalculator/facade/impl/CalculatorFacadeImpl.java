package com.stratio.phonebillcalculator.facade.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stratio.phonebillcalculator.core.component.CalculatorComponent;
import com.stratio.phonebillcalculator.core.entity.LongestCall;
import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.core.exceptions.CalculatorJobException;
import com.stratio.phonebillcalculator.core.exceptions.InvalidTimeFormat;
import com.stratio.phonebillcalculator.facade.CalculatorFacade;
import com.stratio.phonebillcalculator.job.CalculatorJobFactory;

@Component
public class CalculatorFacadeImpl implements CalculatorFacade {

	@Autowired
	private CalculatorJobFactory jobFactory;

	@Autowired
	private CalculatorComponent component;

	public PhoneBillCalculation createJob(String path) throws CalculatorJobException {
		return jobFactory.createJob(path);
	}

	public LongestCall findOutLongerCall(List<PhoneBillLineCalculation> list) {
		return component.findOutLongestCall(list);
	}

	public int getSecondsFromString(String time) throws InvalidTimeFormat {
		return component.getSecondsFromString(time);
	}

	public BigDecimal calculatePhoneBillLineCost(int length) {
		return component.calculatePhoneBillLineCost(length);
	}

	public boolean validateTelephoneNumber(String telephoneNumber) {
		return component.validateTelephoneNumber(telephoneNumber);
	}

	public PhoneBillCalculation calculateTotalCosts(List<PhoneBillLineCalculation> list) {
		return component.calculateTotalCosts(list);
	}
}