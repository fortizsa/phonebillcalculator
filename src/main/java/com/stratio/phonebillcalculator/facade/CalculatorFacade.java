package com.stratio.phonebillcalculator.facade;

import java.math.BigDecimal;
import java.util.List;

import com.stratio.phonebillcalculator.core.entity.LongestCall;
import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.core.exceptions.CalculatorJobException;
import com.stratio.phonebillcalculator.core.exceptions.InvalidTimeFormat;

public interface CalculatorFacade {

	public PhoneBillCalculation createJob(String path) throws CalculatorJobException;

	public LongestCall findOutLongerCall(List<PhoneBillLineCalculation> list);

	public int getSecondsFromString(String time) throws InvalidTimeFormat;

	public BigDecimal calculatePhoneBillLineCost(int length);

	public boolean validateTelephoneNumber(String telephoneNumber);

	public PhoneBillCalculation calculateTotalCosts(List<PhoneBillLineCalculation> list);
}
