package com.stratio.phonebillcalculator.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PhoneBillLineCalculation implements Serializable {

	private static final long serialVersionUID = -2732292047525131344L;

	private int length;
	
	private String telephoneNumber;
	
	private BigDecimal cost;

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
}