package com.stratio.phonebillcalculator.core.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class PhoneBillLine implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6085998497283773358L;

	private String time;

	private String telephoneNumber;
}