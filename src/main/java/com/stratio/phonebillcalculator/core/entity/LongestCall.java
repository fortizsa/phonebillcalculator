package com.stratio.phonebillcalculator.core.entity;

import lombok.Data;

@Data
public class LongestCall {

	private String telephoneNumber;

	private int seconds;
	
	public LongestCall(String telephoneNumber, int seconds) {
		this.telephoneNumber = telephoneNumber;
		this.seconds = seconds;
	}
}