package com.stratio.phonebillcalculator.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PhoneBillCalculation implements Serializable{

	private static final long serialVersionUID = -2228426649695527003L;
	
	private BigDecimal totalCost = BigDecimal.ZERO;

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
}
