package com.stratio.phonebillcalculator.core.component.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.stratio.phonebillcalculator.config.CalculatorConfiguration;
import com.stratio.phonebillcalculator.core.component.CalculatorComponent;
import com.stratio.phonebillcalculator.core.entity.LongestCall;
import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.core.exceptions.InvalidTimeFormat;

/**
 * Include all the core functionalities of the Phone Bill Calculator
 * 
 * @author francisco
 *
 */
@Component
public class CalculatorComponentImpl implements CalculatorComponent {

	private Logger logger = LoggerFactory.getLogger(CalculatorComponentImpl.class);

	private CalculatorConfiguration conf;

	private Pattern timePattern = Pattern.compile("^([0-9]{2}):([0-9]{2}):([0-9]{2})$");

	private Pattern telephoneNumberPattern = Pattern.compile("^([0-9]{3})-([0-9]{3})-([0-9]{3})$");

	public CalculatorComponentImpl(CalculatorConfiguration conf) {
		this.conf = conf;
	}

	/**
	 * @param ret Time pattern HH:mm:ss
	 * @return
	 */
	public int getSecondsFromString(String str) throws InvalidTimeFormat {
		Matcher matcher = timePattern.matcher(str);
		if (matcher.find() && matcher.groupCount() == 3) {
			return this.calculateTotalSeconds(Integer.valueOf(matcher.group(1)), Integer.valueOf(matcher.group(2)),
					Integer.valueOf(matcher.group(3)));
		} else {
			throw new InvalidTimeFormat(String.format("Time format %s not matching to HH:mm:ss format", str));
		}
	}

	public int calculateTotalSeconds(int hours, int minutes, int seconds) {
		return (hours * 60 * 60) + (minutes * 60) + seconds;
	}

	public LongestCall findOutLongestCall(List<PhoneBillLineCalculation> lines) {
		// Group by telephone number
		Map<String, Integer> set = new HashMap<>();
		lines.stream().forEach(c -> {
			if (set.containsKey(c.getTelephoneNumber())) {
				set.put(c.getTelephoneNumber(), set.get(c.getTelephoneNumber()) + c.getLength());
			} else {
				set.put(c.getTelephoneNumber(), c.getLength());
			}
		});

		// Find out longest call
		List<String> telephoneNumbers = new ArrayList<>();
		int maxLength = 0;
		Iterator<String> it = set.keySet().iterator();
		while (it.hasNext()) {
			String tlf = it.next();
			int length = set.get(tlf);
			if (length > maxLength) {
				telephoneNumbers.clear();
				telephoneNumbers.add(tlf);
				maxLength = length;
			} else if (maxLength == length) {
				telephoneNumbers.add(tlf);
			}
		}

		return new LongestCall(findOutTelephoneNumberForTheLongestCall(telephoneNumbers), maxLength);
	}

	/**
	 * Returns the smallest telephonenumber
	 */
	public String findOutTelephoneNumberForTheLongestCall(List<String> telephoneNumbers) {
		String telephoneNumber = null;
		// If more that one number found, return the smallest
		if (telephoneNumbers.size() > 1) {
			// Transform telephone number to an integer and saves both for identify it
			Map<Integer, String> equivalents = new HashMap<>();

			List<Integer> col = telephoneNumbers.stream().map(p -> {
				int tln = (int) Integer.valueOf(p.replace("-", "").trim());
				equivalents.put(tln, p);
				return tln;
			}).collect(Collectors.toList());

			Collections.sort(col);
			telephoneNumber = equivalents.get(col.get(0));
		} else {
			telephoneNumber = telephoneNumbers.get(0);
		}

		return telephoneNumber;
	}

	public BigDecimal calculatePhoneBillLineCost(int seconds) {
		return seconds < conf.getLength() ? conf.getCost1().multiply(new BigDecimal(seconds))
				: conf.getCost2().multiply(new BigDecimal(this.getCeilMinutes(seconds)));
	}

	public int getCeilMinutes(int seconds) {
		return seconds > 0 ? (int) Math.ceil((double) seconds / (double) 60) : 0;
	}

	public boolean validateTelephoneNumber(String telephoneNumber) {
		return telephoneNumberPattern.matcher(telephoneNumber).matches();
	}

	public PhoneBillCalculation calculateTotalCosts(List<PhoneBillLineCalculation> list) {
		LongestCall call = findOutLongestCall(list);
		PhoneBillCalculation calculation = new PhoneBillCalculation();
		list.stream().forEach(p -> {
			if (!p.getTelephoneNumber().equals(call.getTelephoneNumber())) {
				calculation.setTotalCost(calculation.getTotalCost().add(p.getCost()));
			}
		});

		if (call != null && call.getTelephoneNumber() != null) {
			logger.debug("Promotion detected: {}", call.getTelephoneNumber());
		}

		return calculation;
	}
}