package com.stratio.phonebillcalculator.core.component;

import java.math.BigDecimal;
import java.util.List;

import com.stratio.phonebillcalculator.core.entity.LongestCall;
import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.core.exceptions.InvalidTimeFormat;

public interface CalculatorComponent {

	public int getSecondsFromString(String str) throws InvalidTimeFormat;
	
	public int calculateTotalSeconds(int hours, int minutes, int seconds);
	
	public LongestCall findOutLongestCall(List<PhoneBillLineCalculation> lines);
	
	public String findOutTelephoneNumberForTheLongestCall(List<String> telephoneNumbers);
	
	public BigDecimal calculatePhoneBillLineCost(int seconds);
	
	public int getCeilMinutes(int seconds);
	
	public boolean validateTelephoneNumber(String telephoneNumber);
	
	public PhoneBillCalculation calculateTotalCosts(List<PhoneBillLineCalculation> list);
}
