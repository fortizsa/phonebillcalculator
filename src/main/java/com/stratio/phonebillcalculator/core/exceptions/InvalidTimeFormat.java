package com.stratio.phonebillcalculator.core.exceptions;

public class InvalidTimeFormat extends Exception{

	private static final long serialVersionUID = -9003865757670165163L;

	public InvalidTimeFormat(String s){
		super(s);
	}
	
	public InvalidTimeFormat(Exception e) {
		super(e);
	}
}
