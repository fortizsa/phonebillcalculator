package com.stratio.phonebillcalculator.core.exceptions;

public class CalculatorJobException extends Exception{

	private static final long serialVersionUID = -1913514725178929219L;

	public CalculatorJobException(Exception e) {
		super(e);
	}
}
