package com.stratio.phonebillcalculator;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.job.CalculatorJobFactory;

@SpringBootApplication
public class PhonebillcalculatorApplication implements CommandLineRunner {

	@Autowired
	private CalculatorJobFactory jobFactory;
	
	private static Logger logger = LoggerFactory.getLogger(PhonebillcalculatorApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PhonebillcalculatorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (args != null && args.length > 0) {
			logger.info("Executing calculator batch for file {}", args[0]);

			String path = args[0];
			File file = new File(path);
			if (file.exists() && !file.isDirectory()) {
				PhoneBillCalculation result = jobFactory.createJob(path);
				logger.info("Phone bill calculation: {}", result.getTotalCost());
			}else {
				logger.error("File {} not found", path);
			}

		} else {
			logger.error("Input argument validation: File path is required");
		}
	}
}