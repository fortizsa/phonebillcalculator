package com.stratio.phonebillcalculator.job;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;

public class CalculatorItemWriter implements ItemWriter<PhoneBillLineCalculation>, Closeable {

	private StepExecution stepExecution;

	@Override
	public void close() throws IOException {
		// Nothing to close
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends PhoneBillLineCalculation> items) throws Exception {
		List<PhoneBillLineCalculation> list = (List<PhoneBillLineCalculation>) this.stepExecution.getJobExecution()
				.getExecutionContext().get(CalculatorJobConstants.JOB_EXECUTION_CONTEXT_LIST);
		
		// Others threads could update same list
		Collections.synchronizedList(list).addAll(items);
	}

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}
}