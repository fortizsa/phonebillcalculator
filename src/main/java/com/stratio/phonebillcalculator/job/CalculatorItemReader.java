package com.stratio.phonebillcalculator.job;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;

import com.stratio.phonebillcalculator.core.entity.PhoneBillLine;

public class CalculatorItemReader extends FlatFileItemReader<PhoneBillLine> {

	public CalculatorItemReader() {
		this.setStrict(false);
		this.setLinesToSkip(0);
		this.setLineMapper(lineMapper());
	}

	@BeforeStep
	public void beforeStep(StepExecution execution) {
		this.setResource(new FileSystemResource((String) execution.getJobParameters().getParameters().get("path").getValue()));
	}

	public LineMapper<PhoneBillLine> lineMapper() {
		DefaultLineMapper<PhoneBillLine> lineMapper = new DefaultLineMapper<>();
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setNames(CalculatorJobConstants.FIELD_1, CalculatorJobConstants.FIELD_2);
		lineTokenizer.setIncludedFields(0, 1);
		BeanWrapperFieldSetMapper<PhoneBillLine> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(PhoneBillLine.class);
		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		return lineMapper;
	}
}
