package com.stratio.phonebillcalculator.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stratio.phonebillcalculator.core.component.impl.CalculatorComponentImpl;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLine;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.facade.CalculatorFacade;

@Component
public class PhoneBillLineProcessor implements ItemProcessor<PhoneBillLine, PhoneBillLineCalculation> {

	private Logger logger = LoggerFactory.getLogger(PhoneBillLineProcessor.class);

	@Autowired
	private CalculatorFacade calculatorFacade;
	
	@Override
	public PhoneBillLineCalculation process(PhoneBillLine item) throws Exception {
		int length = this.calculatorFacade.getSecondsFromString(item.getTime());
		
		PhoneBillLineCalculation ret = new PhoneBillLineCalculation();
		ret.setLength(length);
		ret.setCost(this.calculatorFacade.calculatePhoneBillLineCost(length));
		
		logger.debug("Phone bill call log: {}, {}, {} , {}", item.getTelephoneNumber(), item.getTime(), length, ret.getCost());
		this.calculatorFacade.validateTelephoneNumber(item.getTelephoneNumber());
		ret.setTelephoneNumber(item.getTelephoneNumber());
		return ret;
	}
}