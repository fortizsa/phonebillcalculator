package com.stratio.phonebillcalculator.job;

import java.util.ArrayList;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class CalculatorJobExecutionListener implements JobExecutionListener {

	@Override
	public void beforeJob(JobExecution jobExecution) {
		jobExecution.getExecutionContext().put(CalculatorJobConstants.JOB_EXECUTION_CONTEXT_LIST, new ArrayList<>());
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		// Nothing
	}
}
