package com.stratio.phonebillcalculator.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.stratio.phonebillcalculator.core.entity.PhoneBillLine;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;

@Component
public class CalculatorJobComponent {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private CalculatorEndTasklet calculatorEndTasklet;

	@Autowired
	private PhoneBillLineProcessor phoneBillLineProcessor;

	@Bean
	@Qualifier("calculatorJob")
	public Job calculatorJob() {
		return jobBuilderFactory.get(CalculatorJobConstants.JOB_NAME).listener(listener())
				.incrementer(new RunIdIncrementer()).start(step1()).next(step2()).build();
	}

	@Bean
	public JobExecutionListener listener() {
		return new CalculatorJobExecutionListener();
	}

	@Bean
	public Step step1() {
		return stepBuilderFactory.get("step").<PhoneBillLine, PhoneBillLineCalculation>chunk(5).reader(reader())
				.processor(phoneBillLineProcessor).writer(writer()).build();
	}

	@Bean
	public Step step2() {
		return stepBuilderFactory.get("step").tasklet(calculatorEndTasklet).build();
	}

	@Bean
	public CalculatorItemWriter writer() {
		return new CalculatorItemWriter();
	}

	@Bean
	public FlatFileItemReader<PhoneBillLine> reader() {
		return new CalculatorItemReader();
	}
}