package com.stratio.phonebillcalculator.job;

public final class CalculatorJobConstants {

	public static final String JOB_NAME = "calculatorJob";
	
	public static final String JOB_EXECUTION_CONTEXT_LIST = "list";
	
	public static final String JOB_EXECUTION_CALCULATION = "calculation";

	public static final String FIELD_1 = "time";

	public static final String FIELD_2 = "telephoneNumber";

	private CalculatorJobConstants() {
		// No instance
	}
}