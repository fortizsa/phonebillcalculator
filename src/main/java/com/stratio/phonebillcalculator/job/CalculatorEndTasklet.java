package com.stratio.phonebillcalculator.job;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.entity.PhoneBillLineCalculation;
import com.stratio.phonebillcalculator.facade.CalculatorFacade;

@Component
public class CalculatorEndTasklet implements Tasklet, StepExecutionListener {

	@Autowired
	private CalculatorFacade calculatorFacade;

	private List<PhoneBillLineCalculation> list;

	private PhoneBillCalculation calculation;

	@SuppressWarnings("unchecked")
	@Override
	public void beforeStep(StepExecution stepExecution) {
		// Recover data from job execution context
		this.list = (List<PhoneBillLineCalculation>) stepExecution.getJobExecution().getExecutionContext()
				.get(CalculatorJobConstants.JOB_EXECUTION_CONTEXT_LIST);
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().put(CalculatorJobConstants.JOB_EXECUTION_CALCULATION,
				this.calculation);
		return ExitStatus.COMPLETED;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		calculation = calculatorFacade.calculateTotalCosts(this.list);
		return RepeatStatus.FINISHED;
	}
}