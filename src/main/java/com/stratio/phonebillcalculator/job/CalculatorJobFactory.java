package com.stratio.phonebillcalculator.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.stratio.phonebillcalculator.core.entity.PhoneBillCalculation;
import com.stratio.phonebillcalculator.core.exceptions.CalculatorJobException;

@Component
public class CalculatorJobFactory {

	@Autowired
	@Qualifier("calculatorJob")
	private Job calculatorJob;
	
	@Autowired
	private JobLauncher jobLauncher;
	
	public PhoneBillCalculation createJob(String path) throws CalculatorJobException {
		PhoneBillCalculation ret = null;
		JobParameters jobParameters = new JobParametersBuilder().addString("path", path).toJobParameters();
		try {
			JobExecution jobExecution = jobLauncher.run(calculatorJob, jobParameters);
			ret = (PhoneBillCalculation) jobExecution.getExecutionContext()
					.get(CalculatorJobConstants.JOB_EXECUTION_CALCULATION);
		} catch (Exception e) {
			throw new CalculatorJobException(e);
		}

		return ret;
	}
}
