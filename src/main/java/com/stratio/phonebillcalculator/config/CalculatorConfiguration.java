package com.stratio.phonebillcalculator.config;

import java.math.BigDecimal;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "calculator")
public class CalculatorConfiguration {

	private int chunks;
	
	private BigDecimal cost1;
	
	private BigDecimal cost2;
	
	private int length;
}