# Phone bill Calculator

Spring boot cli application using batch framework  
Recieves a phone bill log and calculate total calls cost

### Requirements 📋

Java 11, Maven 3.8 minimum

### Install 🔧

```
mvn clean package
```

Avoid test execution

```
mvn clean package -Dmaven.test.skip=true
```

## Execute tests

mvn test

## Execute application

Use phone bill call log file as first argument

```
java -jar target/phonebillcalculator-1.0.jar /Users/francisco/repositorio/stratio/phonebillcalculator/src/test/resources/testdata.txt
```

## Author

Created by fortizsa

## License 📄

Feel free to share